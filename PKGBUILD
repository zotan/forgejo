# Maintainer: George Rawlinson <grawlinson@archlinux.org>

pkgname=forgejo
pkgver=7.0.3
pkgrel=1
pkgdesc='A lightweight software forge'
arch=('x86_64')
url='https://forgejo.org'
license=('MIT')
depends=('glibc' 'git')
makedepends=('go' 'nodejs' 'npm' 'python-poetry')
checkdepends=('openssh')
optdepends=(
  'mariadb: MariaDB support'
  'memcached: MemCached support'
  'openssh: GIT over SSH support'
  'pam: Authentication via PAM support'
  'postgresql: PostgreSQL support'
  'redis: Redis support'
  'sqlite: SQLite support'
)
backup=('etc/forgejo/app.ini')
options=('!lto' '!debug')
source=(
  "$pkgname::git+https://codeberg.org/forgejo/forgejo#tag=v$pkgver"
  'systemd.service'
  'sysusers.conf'
  'tmpfiles.conf'
)
sha512sums=('a1711bf26e9b753444ba886b7313753431720164bdc6759657bbfe7da05509575060f8807e1e759dbfecfbfd4e9d5bdc48654f0105743dd3a014b151eef066a0'
            '90684aa56930f40ff791fb4789375bbcf69cecc98c3a9fd3764c6f53e60bb32ddedd83cd75c00ff9d6aa90741361815dc7e23904ac355075b0f0990853639198'
            '933c0b6cee9d32d2b11a8ee2476e20c5f06a089ed4310ffb24155902f308b4fb447b3b2f3bf7dee76be24475873da5bd026c4823cd56590b20bdf2805f4d07a8'
            '9a3aa163892eaa889e74d066db9d620db098535b08fa51df689e7aa5885393a14b820308364196db54d7ce502791ea56b662d8aede17fad99f8f62d1a3ca6776')
b2sums=('5427f65b26a65829da29cbccaa283ef8b3966d8d05c246448b566a813f3267a26237e5fa6595d055429994527621d777695a62b225d3918a8ed761f7f6cb28b5'
        '6c51a7d121dc365c9839d5a6cb3d0edf36018f0ca34c5e9b21dc8f45ea21fa05971f12aedc5af6dcc5c1bd81f7430ab4df1e63f8cd19e1dfcb8088538edf9c15'
        'cab7b5cbf24242e3d941725111de012c0ee16495e8f367ab48cb02b2261133c262a1704f58b20fd08ec626bcb23cef2473b55e19dbf7db60c243b1f30fd13743'
        'f48abdd1d207ee61b1f688e2050adaac30eb19514d1d7ef095cf2e92326f4218a16e37a8910d9eeabc17fda2dbf2e4709961560b5ecd3f06187eddde3312702c')

build() {
  cd "$pkgname"

  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export EXTRA_GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw"
  export LDFLAGS="-X 'code.gitea.io/gitea/modules/setting.AppWorkPath=/var/lib/forgejo/' -X 'code.gitea.io/gitea/modules/setting.CustomConf=/etc/forgejo/app.ini'"
  export TAGS="bindata sqlite sqlite_unlock_notify pam"

  make build
}

package() {
  # systemd integration
  install -vDm644 systemd.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -vDm644 sysusers.conf "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
  install -vDm644 tmpfiles.conf "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"

  cd "$pkgname"

  # binary
  install -vDm755 gitea "$pkgdir/usr/bin/$pkgname"

  # configuration
  install -vDm644 custom/conf/app.example.ini "$pkgdir/etc/$pkgname/app.ini"

  # license
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE
}
